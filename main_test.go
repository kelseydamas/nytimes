package main

import (
	"testing"
)

func Test_is_solution(t *testing.T) {
	type args struct {
		candidate string
		letters   []byte
		required  rune
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "false test",
			args: args{
				candidate: "abcdef",
				required:  'a',
				letters:   []byte("abcde"),
			},
			want: false,
		},
		{
			name: "false test 2",
			args: args{
				candidate: "abcdef",
				required:  'g',
				letters:   []byte("ghijklkm"),
			},
			want: false,
		},
		{
			name: "perfect match",
			args: args{
				candidate: "abcdef",
				required:  'a',
				letters:   []byte("abcdef"),
			},
			want: true,
		},
		{
			name: "too short",
			args: args{
				candidate: "abc",
				required:  'a',
				letters:   []byte("abcdef"),
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := is_solution(tt.args.candidate, tt.args.letters, tt.args.required); got != tt.want {
				t.Errorf("is_solution() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_mainErr(t *testing.T) {
	type args struct {
		wordlist string
		required string
		chars    string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "bunk test",
			args: args{
				wordlist: "/usr/share/dict/words",
				required: "a",
				chars:    "abcdef",
			},
			wantErr: false,
		},
		{
			name: "bad file",
			args: args{
				wordlist: "/dev/mem",
				required: "a",
				chars:    "abcdef",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := mainErr(&tt.args.wordlist, &tt.args.required, &tt.args.chars); (err != nil) != tt.wantErr {
				t.Errorf("mainErr() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
