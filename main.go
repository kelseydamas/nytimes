package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gopkg.in/alecthomas/kingpin.v2"
)

func is_solution(candidate string, letters []byte, required rune) bool {
	if len(candidate) < 4 {
		//log.Printf("%s is too short", candidate)
		return false
	}
	if !strings.ContainsRune(candidate, required) {
		//log.Printf("%s missing required rune", candidate)
		return false
	}
	// does the candidate word contain any letters we don't have?
	for i := 0; i < len(candidate); i++ {
		c_invalid := true
		for j := 0; j < len(letters); j++ {
			if candidate[i] == letters[j] {
				c_invalid = false
				break
			}
		}
		if c_invalid {
			//log.Printf("%s has the letter %c which is not found in letters %s\n", candidate, candidate[i], letters)
			return false
		}
	}
	//log.Printf("%s is a solution\n", candidate)
	return true
}

var (
	required = kingpin.Flag("required", "required character.").Required().String()
	chars    = kingpin.Flag("chars", "list of characters in puzzle").Required().String()
	wordlist = kingpin.Flag("wordlist", "Path to word list").Default("/usr/share/dict/words").ExistingFile()
)

func mainErr(wordlist *string, required *string, chars *string) error {
	file, err := os.Open(*wordlist)
	if err != nil {
		return err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		candidate := strings.ToLower(scanner.Text())
		requiredVal := *required
		if is_solution(candidate, []byte(*chars), rune(requiredVal[0])) {
			fmt.Printf("%s\n", candidate)
		}
	}
	return nil
}

func main() {
	kingpin.CommandLine.Help = "A brute force solver for the NYT word game"
	kingpin.Parse()
	err := mainErr(wordlist, required, chars)
	if err != nil {
		os.Exit(1)
	}
}
